/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double a) {
	this->a = a;//incorrect assignment fixed.Q(6)
	this->b = a;//logic bug fixed.Q(6)
}

void Square::setB(double b) {
	this->b = b;//incorrect assignment fixed.Q(6)
	this->a = b; //logic bug fixed.Q(6)
}

double Square::calculateCircumference() {
	return (a + b) * 2; //incorrect circumference calculation fixed.Q(6)
}

double Square::calculateArea() {
	return a * b;
}
