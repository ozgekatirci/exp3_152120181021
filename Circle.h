/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:

	Circle(double);
	~Circle();//virtual unnecessary so removed.Q(6)
	void setR(double);
	void setR(int);//Overload the setR .Q(5)
	double getR()const;//Q(2)
	double calculateCircumference()const;//Q(2)
	double calculateArea()const;//Q(2)
	bool equal(const Circle&);//definition function that compare circle4 and circle5.Q(3)
private:
	double r;
	const double PI = 3.14;//to make it unchangeable.Q(4)

};
#endif /* CIRCLE_H_ */
