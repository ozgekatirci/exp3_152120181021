/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <iostream>
using namespace std;
// to fixed bugs namespace Student1 removed.(Q1)
Circle::Circle(double r) {
	setR(r);
}

Circle::~Circle() {
}

void Circle::setR(int r) {//Overload the setR .Q(5)
	this->r = r;
}

void Circle::setR(double r) {
	this->r = r;
}

double Circle::getR()const {//Q(2)
	return r;
}

double Circle::calculateCircumference()const {//Q(2)
	return PI * r * 2;///incorrect circumference calculation fixed.Q(6)
}

double Circle::calculateArea()const {//Q(2)
	return PI * r * r;// incorrect area calculation fixed.Q(6)
}
bool Circle::equal(const Circle& c2) {// function to compare circle4 and circle5.Q(3)

	if (calculateCircumference() == c2.calculateCircumference() && calculateArea() == c2.calculateArea()) {
		cout << "They are equal." << endl;
		return true;
	}
	else {
		cout << "They aren't equal." << endl;
		return false;
	}

}


