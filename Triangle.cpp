/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a) {//function parameter name added.Q(6)
	this->a = a;//incorrect assignment fixed.Q(6)
}

void Triangle::setB(double b) {//function parameter name added.Q(6)
	this->b = b;//incorrect assignment fixed.Q(6)
}

void Triangle::setC(double c) {//function parameter name added.Q(6)
    this->c= c;//incorrect assignment fixed.Q(6)
}

double Triangle::calculateCircumference() {
	return a + b + c ;//incorrect circumference calculation fixed.Q(6)
}
